FROM python:3.8 AS builder

WORKDIR /app

RUN pip install poetry
ENV PATH "/root/.poetry/bin:${PATH}"

ADD . /app

RUN poetry build
RUN poetry export --without-hashes -f requirements.txt > ./dist/requirements.txt

FROM python:3.8

WORKDIR /tmp

COPY --from=builder /app/dist .

RUN python -m venv "${HOME}/venv"
RUN . "${HOME}/venv/bin/activate" && pip install -r requirements.txt --find-links=. jenkins-monitor

CMD . "${HOME}/venv/bin/activate" && jenkins-monitor
