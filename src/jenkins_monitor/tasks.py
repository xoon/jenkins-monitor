import traceback

from typing import Optional, List

import asyncio
import functools
import logging
import time

from prometheus_client import Gauge, Summary, Counter

from jenkins_monitor import jenkins, es
from jenkins_monitor.es import BulkItem, BulkActionEnum
from jenkins_monitor.jenkins import BuildSummary, JobSummary, Build

logger = logging.getLogger(__name__)

JENKINS_JOBS_TOTAL = Gauge(
    'jenkins_jobs_total',
    'Number of jobs in Jenkins',
    ['buildable']
)
JENKINS_QUEUE_TOTAL = Gauge(
    'jenkins_queue_total',
    'Number of jobs in Jenkins',
    ['status']
)
JENKINS_QUEUE_WAITING_TOTAL = Gauge(
    'jenkins_queue_waiting_total_duration_seconds',
    'Cumulative waiting time of queue items',
    ['status']
)
JENKINS_QUEUE_WAITING_MAX = Gauge(
    'jenkins_queue_waiting_max_duration_spasseconds',
    'Cumulative waiting',
    ['status']
)

SCHEDULED_TASKS = Summary(
    'jenkinsmonitor_scheduled_tasks',
    'Scheduled tasks',
    ['name', 'status']
)

SCHEDULED_TASKS_TIMEOUT = Counter(
    'jenkinsmonitor_scheduled_tasks_timeout',
    'Scheduled tasks timeout',
    ['name']
)


def schedule(seconds):
    def wrapper(coro):
        @functools.wraps(coro)
        async def wrapped(*args):
            while True:
                logger.info(f"schedule: {coro.__name__} start")

                status = 'failure'
                start = time.time()
                try:
                    await asyncio.wait_for(coro(), seconds)
                    status = 'success'
                except asyncio.TimeoutError:
                    logger.warning(f"schedule: timeout for {coro.__name__}")
                    SCHEDULED_TASKS_TIMEOUT.labels(name=coro.__name__).inc(1)
                except Exception:
                    logger.critical(f"{coro.__name__}: {traceback.format_exc()}")
                stop = time.time()

                logger.info(f"schedule: {coro.__name__} {status} "
                            f"after {stop - start:.2f}s")
                (SCHEDULED_TASKS.labels(name=coro.__name__, status=status)
                                .observe(stop - start))

                await asyncio.sleep(seconds - min(seconds, (stop - start)))
        return wrapped
    return wrapper


@schedule(5.0)
async def quick_scan_jobs():
    jobs = await jenkins.get_jobs()
    buildable_counts = [0, 0]
    for job in jobs:
        buildable_counts[job.buildable] += 1
    for index, value in enumerate(buildable_counts):
        JENKINS_JOBS_TOTAL.labels(buildable=index).set(value)
    return


@schedule(5.0)
async def quick_scan_queue():
    now = time.time()
    items = await jenkins.get_queue_items()
    logger.debug(f'quick_scan_queue: {len(items)} queue items')
    statuses_stats = {
        status: {'count': 0, 'waiting': []}
        for status in jenkins.JENKINS_QUEUE_STATUSES
    }
    for item in items:
        statuses_stats[item.status]['count'] += 1
        statuses_stats[item.status]['waiting'].append(item.inQueueSince)
    for status, stats in statuses_stats.items():
        waiting_total = 0.0
        waiting_max = 0.0
        if stats['count']:
            waiting_total = (now * len(stats['waiting'])) - (sum(stats['waiting']) / 1000.0)
            waiting_max = now - (max(stats['waiting']) / 1000.0)

        JENKINS_QUEUE_WAITING_TOTAL.labels(status=status).set(waiting_total)
        JENKINS_QUEUE_WAITING_MAX.labels(status=status).set(waiting_max)
        JENKINS_QUEUE_TOTAL.labels(status=status).set(stats['count'])
    return


@schedule(30)
async def scan_unknown_builds():
    jenkins_jobs = await jenkins.get_jobs()
    latest_known_builds = await es.get_latest_job_builds()

    for job in jenkins_jobs:
        last_number = job.lastBuild.number
        known: Build = latest_known_builds.get(job.name, None)

        if known and known.id == str(last_number):
            logging.debug(f"Latest build {job.lastBuild.number} for {job.name} already known")
            continue

        start = int(known.id) if known else 0
        builds = await jenkins.get_job_builds(job.url, start=start)
        logger.info(f'scan_unknown_builds: scanning {len(builds)} of {job.name}')
        for build in builds:
            await scan_new_build(job, build.url)
    return


@schedule(30)
async def scan_running_builds():
    running_builds = await es.get_running_builds()
    for build in running_builds:
        await scan_existing_build(build)
    return


async def scan_existing_build(build: Build):
    current = await jenkins.get_job_build(build.url)
    current.job = build.job
    items = get_build_es_bulk_items(current, build)
    await es.bulk(items)


async def scan_new_build(job: JobSummary, build_url: str):
    build = await jenkins.get_job_build(build_url)
    build.job = job.name
    items = get_build_es_bulk_items(build)
    await es.bulk(items)


def get_build_es_bulk_items(build: Build, latest: Optional[Build] = None) -> List[BulkItem]:
    index = 'jenkins-builds'
    ts_start = build.timestamp
    duration = build.duration or (round(time.time() * 1000) - ts_start)

    items = [
        {"action": BulkActionEnum.update,
         "timestamp": ts_start + duration,
         "event": "latest",
         "building": 0}
    ]
    if not latest or build.result:
        items.append({"action": BulkActionEnum.update,
                      "timestamp": ts_start - 11000,
                      "event": "pre-start",
                      "building": 0})
        items.append({"action": BulkActionEnum.update,
                      "timestamp": ts_start,
                      "event": "start"})
        next_update = build.timestamp + (60000 - build.timestamp % 60000)
    else:
        next_update = latest.timestamp + (60000 - latest.timestamp % 60000)

    if duration > 11000:
        items.append({"action": BulkActionEnum.update,
                      "timestamp": ts_start + duration - 11000,
                      "event": "pre-latest",
                      "building": 1})

    # how many minutes between build timestamp and next planned minute mark
    updates = 1 + ((ts_start + duration - next_update) // 60000)
    for i in range(updates):
        items.append({'action': BulkActionEnum.update,
                      "timestamp": next_update + (i * 60000),
                      "building": 1})

    bulk_items = [
        get_doc_bulk_item(index=index, doc=build, result=build.result, duration=duration,
                          **params)
        for params in items
    ]
    return bulk_items


def get_doc_bulk_item(action: BulkActionEnum, index: str, doc: Build,
                      timestamp: int, duration: int, event: Optional[str] = None,
                      building: int = 1, result: str = None):
    item = doc.copy(deep=True)
    item.timestamp = timestamp
    item.event = event
    item.building = building
    item.result = result
    item.duration = duration
    bulk_item = BulkItem(action=action, index=index, doc=item, id=item._id)
    return bulk_item


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(scan_unknown_builds())
    loop.run_until_complete(es.client.transport.close())
