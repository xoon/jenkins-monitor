import time
from typing import Tuple
from urllib.parse import parse_qs

from fastapi import APIRouter
from starlette.middleware.base import BaseHTTPMiddleware
from starlette.requests import Request
from starlette.responses import Response

from prometheus_client import Summary, registry
from prometheus_client.exposition import choose_encoder


router = APIRouter()

REQUEST_DURATION = Summary(
    'http_request_duration_seconds',
    'Time spent processing requests',
    ['method', 'endpoint', 'status']
)


class RequestsMetricsMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request, call_next):
        start = time.time()
        response = await call_next(request)
        endpoint = '/'.join(request.url.path.split('/')[0:2])
        (REQUEST_DURATION
            .labels(method=request.method, endpoint=endpoint, status=response.status_code)
            .observe(time.time() - start))
        return response


def generate_metrics(accept_header: str, params: str) -> Tuple[str, str]:
    params = parse_qs(params)
    _registry = registry.REGISTRY
    if 'name[]' in params:
        _registry = registry.restricted_registry(params['name[]'])
    encoder, content_type = choose_encoder(accept_header)
    output = encoder(_registry)
    return output, content_type[1]


@router.get('/metrics')
async def metrics(request: Request):
    accept_header = request.headers.get('Accept')
    params = str(request.query_params)
    content, content_type = generate_metrics(accept_header, params)
    return Response(content=content, media_type=content_type)
