import asyncio
from typing import Callable

from fastapi import APIRouter
from pydantic import BaseModel
from starlette.requests import Request

tests = []
router = APIRouter()


class SmokeTestResult(BaseModel):
    name: str
    state: bool
    message: str


def register(func: Callable[..., SmokeTestResult]) -> Callable:
    tests.append(func)
    return func


@router.get('/smokeTest')
async def smoke_test(request: Request):
    results = await asyncio.gather(
        *[func() for func in tests],
        return_exceptions=True
    )
    return results
