import traceback

import asyncio
import logging
from multiprocessing import Process

import fastapi
import uvicorn
from aiohttp import ClientResponseError
from uvicorn.loops.auto import auto_loop_setup
from fastapi import FastAPI, HTTPException
from starlette.responses import Response
from starlette.requests import Request

from jenkins_monitor import metrics, jenkins
from jenkins_monitor import smoketest
from jenkins_monitor import tasks
from jenkins_monitor import es

app = FastAPI()

app.include_router(metrics.router)
app.add_middleware(metrics.RequestsMetricsMiddleware)

app.include_router(smoketest.router)

background_tasks = []


@app.get("/ping")
async def ping():
    return "OK"


@app.get("/")
async def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
async def read_item(item_id: int, q: str = None):
    return {"item_id": item_id, "q": q}


@app.post("/jenkins/jobs/{job}")
async def scan_job(job: str):
    try:
        _job = await jenkins.get_job(job)
        builds = await jenkins.get_job_builds(_job.url)
        for build in builds:
            await es.delete_build_docs(_job.name, str(build.number))
            await tasks.scan_new_build(_job, build.url)
    except ClientResponseError as err:
        if err.status == 404:
            raise HTTPException(status_code=404, detail="Job not found")
        raise HTTPException(status_code=500, detail=traceback.format_exc())


@app.post("/jenkins/jobs/{job}/builds/{build}")
async def scan_job_build(job: str, build: str):
    try:
        _job = await jenkins.get_job(job)
        await es.delete_build_docs(_job.name, build)
        _build = await jenkins.get_job_build(f'/job/{_job.name}/{build}')
        await tasks.scan_new_build(_job, _build.url)
    except ClientResponseError as err:
        if err.status == 404:
            raise HTTPException(status_code=404, detail="Job not found")
        raise HTTPException(status_code=500, detail=traceback.format_exc())


@app.delete("/jenkins/jobs/{job}")
async def delete_job(job: str):
    await es.delete_build_docs(job)


@app.delete("/jenkins/jobs/{job}/builds/{build}")
async def delete_job_build(job: str, build: str):
    await es.delete_build_docs(job, build)


@app.get("/jenkins/jobs/{job}/builds")
async def get_job_builds(job: str):
    builds = await es.get_builds(None, None)
    return builds


@app.post("/cleanup")
async def cleanup():
    body = {
        "size": 10000,
        "query": {
            "bool": {
                "must_not": [
                    {"exists": {"field": "job"}}
                ]
            }
        }
    }
    return await es.delete_by_query(index="jenkins-builds", body=body)


@app.on_event("startup")
async def start_tasks():
    background_tasks.append(asyncio.create_task(es.setup_client(), name="es client"))
    background_tasks.append(asyncio.create_task(es.create_indices(), name="es indices"))
    background_tasks.append(asyncio.create_task(tasks.quick_scan_jobs(), name="jenkins jobs"))
    background_tasks.append(asyncio.create_task(tasks.quick_scan_queue(), name="jenkins queue"))
    background_tasks.append(asyncio.create_task(tasks.scan_unknown_builds(), name="jenkins unknown builds"))
    background_tasks.append(asyncio.create_task(tasks.scan_running_builds(), name="jenkins running builds"))


@app.on_event("shutdown")
async def stop():
    es.client.transport.close()


def main():
    logging.basicConfig(level=logging.DEBUG)
    try:
        uvicorn.run(app, host="0.0.0.0", port=8000, loop="asyncio")
    finally:
        for task in background_tasks:
            task.cancel()


if __name__ == "__main__":
    logging.basicConfig(level=logging.WARNING)
    main()
