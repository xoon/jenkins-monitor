import hashlib

import os
import logging
import time
import traceback
from typing import Optional, List, Union, Literal
from urllib.parse import urlparse

import aiohttp
from prometheus_client import Summary, Counter
from pydantic import BaseModel

from jenkins_monitor import smoketest
from jenkins_monitor.smoketest import SmokeTestResult

JENKINS_URL = os.getenv('JENKINS_URL')
JENKINS_HOST = urlparse(JENKINS_URL).netloc.split(':')[0]
JENKINS_API_USER = os.getenv('JENKINS_USERNAME')
JENKINS_API_TOKEN = os.getenv('JENKINS_API_TOKEN')
JENKINS_AUTH = aiohttp.BasicAuth(JENKINS_API_USER, JENKINS_API_TOKEN)
JENKINS_QUEUE_STATUSES = ['buildable', 'blocked', 'pending', 'stuck']

HTTP_OUT_DURATION = Summary(
    'http_out_request_duration_seconds',
    'Time spent communicating with jenkins',
    ['host', 'endpoint', 'status']
)

HTTP_ERROR = Counter(
    'http_out_request_errors',
    'Failed HTTP requests',
    ['host', 'endpoint']
)


class BuildSummary(BaseModel):
    _class: str
    number: int
    job: Optional[str]
    url: str


class JobSummary(BaseModel):
    _class: str
    name: str
    url: str
    buildable: bool
    lastBuild: BuildSummary


class QueueSummary(BaseModel):
    blocked: bool
    buildable: bool
    pending: Optional[bool] = False
    stuck: bool
    inQueueSince: int

    @property
    def status(self) -> str:
        for _status in JENKINS_QUEUE_STATUSES:
            if getattr(self, _status):
                return _status
        else:
            return "unknown"


class SmokeTest(BaseModel):
    _class: str
    quietingDown: bool


class Parameter(BaseModel):
    _class: str
    name: str
    value: Union[bool, str, None]


class CauseUser(BaseModel):
    _class: Literal["hudson.model.Cause$UserIdCause"]
    userName: str


class Build(BaseModel):
    _class: str
    host: str = JENKINS_HOST
    job: Optional[str]
    building: int
    duration: int
    url: str
    id: str
    result: Optional[str]
    fullDisplayName: str
    timestamp: int
    event: Optional[str]
    parameters: Optional[List[Parameter]]
    causes: Optional[List[Union[CauseUser]]]

    @property
    def _id(self) -> str:
        identifier = f'{self.host}:{self.fullDisplayName}:{self.event or self.timestamp}'
        digest = hashlib.sha1(identifier.encode('utf-8')).hexdigest()
        logging.critical(f"{identifier}: {digest}")
        return digest


async def get(endpoint: str, url: str, params: Optional[dict] = None, timeout: Optional[int] = 10) -> dict:
    url_parts = urlparse(url)
    if not url_parts.netloc:
        url = f"{JENKINS_URL}{url}"
    url = f"{url}/api/json"
    start = time.time()
    status = None
    try:
        async with aiohttp.ClientSession() as session:
            async with session.get(url, params=params, timeout=aiohttp.ClientTimeout(timeout), auth=JENKINS_AUTH) as response:
                status = response.status
                data = await response.json()
                (HTTP_OUT_DURATION
                 .labels(host=JENKINS_HOST, endpoint=endpoint, status=status)
                 .observe(time.time() - start))
                response.raise_for_status()
        return data
    except aiohttp.ClientResponseError as err:
        status = err.status
        raise err
    finally:
        if status:
            (HTTP_OUT_DURATION
                .labels(host=JENKINS_HOST, endpoint=endpoint, status=status)
                .observe(time.time() - start))
        else:
            (HTTP_ERROR
                .labels(host=JENKINS_HOST, endpoint=endpoint)
                .inc(1)
             )


@smoketest.register
async def test() -> SmokeTestResult:
    try:
        response = await get('root', '', params={'tree': 'quietingDown'}, timeout=1)
        SmokeTest.parse_obj(response)
        return SmokeTestResult(name="Jenkins", state=True, message="OK")
    except Exception as err:
        return SmokeTestResult(name="Jenkins", state=False, message=traceback.format_exc())


async def get_jobs() -> List[JobSummary]:
    params = {
        "tree": "jobs[name,url,buildable,lastBuild[number,url]]"
    }
    data = await get('view', '/view/all', params)
    jobs = [JobSummary.parse_obj(obj) for obj in data['jobs']]
    return jobs


async def get_job(name: str) -> JobSummary:
    params = {
        "tree": "name,url,buildable,lastBuild[number,url]"
    }
    data = await get('job', f'/job/{name}', params)
    job = JobSummary.parse_obj(data)
    return job


async def get_queue_items() -> List[QueueSummary]:
    params = {
        "tree": "items[blocked,buildable,pending,stuck,inQueueSince]"
    }
    data = await get('queue', '/queue', params)
    items = [QueueSummary.parse_obj(obj) for obj in data['items']]
    return items


async def get_job_builds(url: str, start: Optional[int] = 0) -> List[BuildSummary]:
    params = {
        "tree": "builds[number,url]"
    }
    data = await get('job', url, params)
    start = start or 0
    items = [BuildSummary.parse_obj(obj) for obj in data['builds'] if not start >= obj['number']]
    return items


async def get_job_build(url: str) -> Build:
    logging.info(f"Scanning {url}")
    params = {
        "tree": "timestamp,url,fullDisplayName,building,duration,id,result,actions[parameters[name,value],causes[userName]]"
    }
    data = await get('build', url, params)
    for action in data['actions']:
        for key in action:
            if action != '_class':
                data[key] = action[key]
    build = Build.parse_obj(data)
    return build
