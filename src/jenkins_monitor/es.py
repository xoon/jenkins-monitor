import time
from enum import Enum

from elasticsearch import helpers, TransportError
from prometheus_client import Counter, Summary
from pydantic import BaseModel, Field
from typing import List, Dict, Any, Union, Optional

import asyncio
import logging
import traceback

from elasticsearch_async import AsyncElasticsearch

from jenkins_monitor import smoketest
from jenkins_monitor.jenkins import Build, BuildSummary, JobSummary
from jenkins_monitor.smoketest import SmokeTestResult

logger = logging.getLogger(__name__)
client = AsyncElasticsearch(hosts=['localhost'], )

INDEX_JENKINS_BUILDS = {
    'index': 'jenkins-builds',
    'body': {
        'mappings': {
            'properties': {
                'host': {'type': 'keyword'},
                'building': {'type': 'long'},
                'duration': {'type': 'long'},
                'id': {'type': 'text'},
                'url': {'type': 'text'},
                'job': {'type': 'keyword'},
                'fullDisplayName': {'type': 'keyword'},
                'result': {'type': 'keyword'},
                'event': {'type': 'keyword'},
                'timestamp': {'type': 'date', 'format': 'epoch_millis'},
                'parameters': {'type': 'nested',
                               'properties': {
                                   'name': {'type': 'keyword'},
                                   'value': {'type': 'text'}
                               }},
                'causes': {'type': 'nested'}
            }
        }
    }
}
INDEX_JENKINS_CONFIGS = {
    'index': 'jenkins_configs_%{+yyyy}',
}
INDEX_JENKINS_STEPS = {
    'index': 'jenkins_steps_%{+yyyy}',
}

HTTP_OUT_DURATION = Summary(
    'jenkinsmonitor_elasticsearch_operation_duration_seconds',
    'Time spent communicating with jenkins',
    ['index', 'operation', 'status']
)

indices_created = False


class BulkActionEnum(str, Enum):
    create = 'create'
    update = 'update'
    delete = 'delete'


class BulkItem(BaseModel):
    action: BulkActionEnum
    index: str
    id: str
    type: str = 'document'
    doc: Build


async def setup_client():
    global client
    client = AsyncElasticsearch(hosts=['localhost'], )


async def print_info():
    info = await client.info()
    print(info)


async def test():
    truc = await client.nodes


async def test_index(index):
    try:
        status, _, _ = await client.indices.exists(index)
        state = (status == 200)
        message = 'OK' if state else 'KO'
        return SmokeTestResult(name="Elastic Search", state=state, message=message)
    except Exception as err:
        return SmokeTestResult(name="Elastic Search", state=False, message=traceback.format_exc())


@smoketest.register
async def test():
    try:
        response = await client.ping()
        return SmokeTestResult(name="Elastic Search", state=True, message="OK")
    except Exception as err:
        return SmokeTestResult(name="Elastic Search", state=False, message=traceback.format_exc())


@smoketest.register
async def test_index_builds():
    return await test_index(INDEX_JENKINS_BUILDS['index'])


@smoketest.register
async def test_index_configs():
    return await test_index(INDEX_JENKINS_CONFIGS['index'])


@smoketest.register
async def test_index_steps():
    return await test_index(INDEX_JENKINS_STEPS['index'])


async def create_indices():
    global indices_created
    await client.indices.create(**INDEX_JENKINS_BUILDS, ignore=400)
    #if 'index-uuid' in index or index.get('acknowledged', False):
    #    indices_created = True


async def delete_indices():
    await client.indices.delete('jenkins-builds', ignore=404)


async def create_doc(index, _id, body, **params):

    coro = client.create(index=index, id=_id, body=body, params=params)
    return await perform_operation(coro, index, 'create', 200)


async def update_doc(index, _id, body):

    coro = client.update(index=index, id=_id, body=body, refresh=True)
    return await perform_operation(coro, index, 'update', 200)


async def perform_search(index, body):

    coro = client.search(index=index, body=body)
    res = await perform_operation(coro, index, 'search', 200)
    return res


async def delete_by_query(index, body):
    coro = client.delete_by_query(index=index, body=body)
    return await perform_operation(coro, index, 'delete_by_query', 200)


async def perform_operation(coro, index, operation, success_status):
    status = success_status
    start = time.time()
    try:
        res = await asyncio.shield(coro)
        return res
    except TransportError as e:
        status = e.status_code
    finally:
        stop = time.time()
        (HTTP_OUT_DURATION
            .labels(index=index, status=status, operation=operation)
            .observe(stop - start))


async def bulk(items: List[BulkItem]):
    # https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-bulk.html
    # unsupported https://github.com/elastic/elasticsearch-py-async/issues/5
    for item in items:
        if item.action == BulkActionEnum.create:
            await create_doc(item.index, item.id, item.doc.dict(by_alias=True))
        elif item.action == BulkActionEnum.update:
            await update_doc(item.index, item.id, item.doc.dict(by_alias=True))
        else:
            logger.critical(f"unsupport bulk action {item.action}")


async def search_builds(body) -> List[Build]:
    builds = []
    res = await perform_search('jenkins-builds', body)

    coll_key = None
    if "collapse" in body:
        coll_key = body["collapse"]["inner_hits"]["name"]

    for hit in res['hits']['hits']:
        if coll_key:
            hit = hit['inner_hits'][coll_key]['hits']['hits'][0]
        build = Build.parse_obj(hit['_source'])
        builds.append(build)
    return builds


async def get_latest_job_builds() -> Dict[str, Build]:
    body = {
        "size": 1000,
        "query": {
            "bool": {
                "must": [
                    {"match": {"host": "localhost"}},
                    {"match": {"event": "latest"}}
                ],
                "filter": [
                    {"term": {"host": "localhost"}},
                    {"term": {"event": "latest"}}
                ]
            }
        },
        "collapse": {
            "field": "job",
            "inner_hits": {
              "name": "most_recent",
              "size": 1,
              "sort": [
                  {"timestamp": "desc"}
              ]
            }
        }
    }
    builds = await search_builds(body)
    return {build.job: build for build in builds}


async def get_running_builds() -> List[Build]:
    body = {
        "size": 1000,
        "query": {
            "bool": {
                "must": [
                    {"match": {"host": "localhost"}},
                    {"match": {"event": "latest"}}
                ],
                "must_not": [
                    {"exists": {"field": "result"}}
                ],
                "filter": [
                    {"term": {"host": "localhost"}},
                    {"term": {"event": "latest"}},
                ]
            }
        },
        "collapse": {
            "field": "fullDisplayName",
            "inner_hits": {
                "name": "most_recent",
                "size": 4,
                "sort": [
                    {"timestamp": "desc"}
                ]
            }
        }
    }
    builds = await search_builds(body)
    return builds


async def delete_build_docs(job: str, build: str = None):
    body = {
        "size": 10000,
        "query": {
            "bool": {
                "must": [
                    {"match": {"host": "localhost"}},
                    {"match": {"job": f"{job}"}}
                ],
                "filter": [
                    {"term": {"host": "localhost"}},
                    {"match": {"job": f"{job}"}}
                ]
            }
        }
    }
    if build:
        body["query"]["bool"]["must"].append({"match": {"id": f"{build}"}})
        body["query"]["bool"]["filter"].append({"match": {"id": f"{build}"}})
    return await delete_by_query(index='jenkins-builds', body=body)


async def get_builds(job: str = None, event: Optional[str] = "latest"):
    body = {
        "size": 10000,
        "query": {
            "bool": {
                "must": [
                    {"match": {"host": "localhost"}},
                ],
                "filter": [
                    {"term": {"host": "localhost"}},
                ]
            }
        }
    }
    if job:
        body["query"]["bool"]["must"].append({"match": {"job": f"{job}"}})
        body["query"]["bool"]["filter"].append({"match": {"job": f"{job}"}})
    if event:
        body["query"]["bool"]["must"].append({"match": {"event": f"{event}"}})
        body["query"]["bool"]["filter"].append({"match": {"event": f"{event}"}})

    builds = await search_builds(body)
    return builds


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    loop = asyncio.get_event_loop()
    # loop.run_until_complete(print_info())
    loop.run_until_complete(delete_indices())
    loop.run_until_complete(create_indices())
    # loop.run_until_complete(get_latest_job_builds())
#    loop.run_until_complete(get_running_builds())
    loop.run_until_complete(client.transport.close())
